#ifndef TBACT_H
#define TBACT_H

//�����, ���������� ��������� ��������� ������. �����������

#include <string>
#include <vector>
#include <opencv\cv.h>

class TBact
{
public:
	TBact(cv::Point center = cv::Point(0, 0), int size = 0, std::string tp = "NoName", cv::Vec3b c = cv::Vec3b(0, 255, 0));
	~TBact();

	double GetTrackDistance();
	cv::Vec3b GetColor();
	int GetSize();
	std::string GetType();
	bool GetAliveStatus() ;		//Moved on last step of tracking?

	void SetColor(cv::Vec3b c);
	void SetColor(uchar r, uchar g, uchar b);
	void SetSize(int size);
	void SetType(std::string tp);
	void SetAliveStatus(bool status) ;
		
	void AddTrackPoint(cv::Point center);
	std::vector<cv::Point> GetTrack(int pointsCount);	//Getting last points of track

	cv::Point GetLastPosition() ;	
	double GetDistance(TBact bact);
private:
	cv::Vec3b color;			//RGB color for visualization
	std::string type;			//May be create special enum?
	int areaSize;				//Count of pixels included to the bacteria area
	std::vector<cv::Point> track;
	bool aliveStatus;
};

#endif