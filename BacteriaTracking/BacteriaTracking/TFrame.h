#ifndef TFRAME_H
#define TFRAME_H

//����� ������ ����� �� �����������������������

#include <opencv\cv.hpp>
#include <opencv2\imgproc.hpp>
#include <opencv2\highgui.hpp>
#include <opencv\highgui.h>
#include <string>

class TFrame
{
public:
	TFrame(int width, int height);
	TFrame(cv::Mat mat);
	TFrame(std::string filename);
	TFrame(TFrame &frame);
	~TFrame();

	//��� ���� ������� ���������� ������� ����� �����������. ������ � TFrame ����� ������� ������ �� TFrame
	cv::Mat GetImage();
	cv::Mat GetProcessedImage();
	void DeleteProcessedImage();

	//Getting background for 
	cv::Mat CalculateBackground(const std::string path, const std::string imgType,
		const int beginImgNumber, const int endImgNumber) ;

	//������ �� ���� ������� ���������� ��� image � ������
	//���� �� ���������� processedImage � ��� processedImage, ���� ��� ����������!!!
	//����� ����� ������� ����� �������� �� ���, �� ���� ����� �� ��� �������������
	//������ � � ������ � �� ������ ������ ������ ���������� ���������� � processedImage
	void Binarization(double maxValue, int adaptiveMethod, int thresholdSize, int blockSize, double C);
	void Binarization(int threshold);	//����������� � �������
	void Erode(int kernelWidth = 3, int kernelHeight = 3, int kernelShape = MorphShapes_c::CV_SHAPE_ELLIPSE);
	void Dilate(int kernelWidth = 3, int kernelHeight = 3, int kernelShape = MorphShapes_c::CV_SHAPE_ELLIPSE);
	void Blur(int kernelWidth = 5, int kernelHeight = 5);
	void MedianBlur(int kernelSize = 5);
	void GaussianBlur(int kernelWidth = 5, int kernelHeight = 5, double sigmaX = 10, double sigmaY = 0);
	void BackgroundCorrection(cv::Mat background, int backgroundError);
private:
	cv::Mat image;			//����������� ��������
	cv::Mat processedImage;	//�������������� �������

	//���������� ������� �� ������� �� �� �� ������� ������, ��� �������� �����������
	cv::Mat GetImagePtr();
	cv::Mat GetProcessedImagePtr();
};

#endif