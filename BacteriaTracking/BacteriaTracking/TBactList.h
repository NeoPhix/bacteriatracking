#ifndef TBACTLIST_H
#define TBACTLIST_H

#include <vector>
#include "TBact.h"
#include "TFrame.h"

class TBactList 
{
public:
	TBactList();
	TBactList(std::vector<TBact> vec);
	~TBactList();

	std::vector<TBact> GetData();

	//������ ��� ���������� ��������. ��� ����������� TFrame ����� �� ����� ������ ��������������,
	//�������� ����� ����� ������ ��� TBactList
	void FrameProcess(TFrame &frame, cv::Mat background, int backgroundError, int threshold);	
	void FindBacteria(TFrame &frame, int minimalBactSize);	
	void UpdateList(TFrame &framePrev, TFrame &frameNext, int minimalBactSize, int maximalDistance,
		int rectWidth, int rectHeight, int transValueX, int transValueY);

	//TODO: Statistic calculation methods 
	double GetAverageTrackDistance();									//In pixels
	double GetAverageTrackDistance(double pixelSize);					//In um
	int GetAverageBacteriaSize();										//In pixels
	double GetAverageBacteriaSize(double pixelSize);					//In um^2
	double GetAverageBacteriaSpeed();									//In pixels by frame
	double GetAverageBacteriaSpeed(double pixelSize, double frameRate);	//In um by time

private:
	std::vector<TBact> data ;

	double GetCorrelationCoefficient(cv::Mat &mat1, cv::Mat &mat2) ;
	TBact FillBact(cv::Mat & mat, cv::Point point, std::string name, cv::Vec3b color);
};

#endif