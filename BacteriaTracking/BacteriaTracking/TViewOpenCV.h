#ifndef TVIEWOPENCV_H
#define TVIEWOPENCV_H

#include <opencv\cv.hpp>
#include <string>
#include "TFrame.h"
#include "TBactList.h"
#include "TBact.h"

enum class BactDesignation
{
	Crosses,
	Points,
	Circles,
	Edges,
};

class TViewOpenCV
{
public:
	TViewOpenCV();
	~TViewOpenCV();

	//All images are represented in RGB color
	static void PaintTrack(cv::Mat &mat, TBact &bact, const int pointsCount, const int thickness);
	static void PaintTracks(cv::Mat &mat, TBactList &bactList, const int pointsCount, const int thickness);
	static void PaintBact(cv::Mat &mat, TBact &bact, const int thickness, 
		const BactDesignation des = BactDesignation::Crosses);
	static void PaintBactSet(cv::Mat &mat, TBactList &bactList, const int thickness,
		const BactDesignation des = BactDesignation::Crosses);
	static void ShowImage(const std::string windowName, const cv::Mat &mat) ;
	static void SaveImage(const std::string fileName, const cv::Mat &mat) ;

	//template <class STDContainer>
	//static void SaveAnimation(const std::string fileName, STDContainer<cv::Mat> matList, 
	//	const int frameDelay = 1000) ;
private:
};

#endif

//template<class STDContainer>
//inline void TViewOpenCV::SaveAnimation(const std::string fileName, STDContainer<cv::Mat> matList, const int frameDelay)
//{
//}
