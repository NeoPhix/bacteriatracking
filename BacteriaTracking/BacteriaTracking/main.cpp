#include <iostream>
#include <vector>
#include <list>
#include <string>
#include <opencv\cv.hpp>
#include <opencv\cxcore.hpp>
#include <opencv\cvaux.hpp>
#include <ctime>

#include "TFrame.h"
#include "TViewOpenCV.h"
#include "TBact.h"
#include "TBactList.h"

void main()
{
	//Parameters
	int backgroundError = 30;
	int threshold = 65;
	int minimalBactSize = 20;
	int maximalDistance = 100;

	//Random number generator initialization
	srand(time(0)) ;		

	//Background calculation
	TFrame framePrev("Data\\14.jpg");
	cv::Mat background = framePrev.CalculateBackground("..\\Data\\", ".jpg", 0, 65) ;
	
	//Search of bacteria on the first image
	TBactList bactList;
	bactList.FrameProcess(framePrev, background, backgroundError, threshold);
	bactList.FindBacteria(framePrev, minimalBactSize);

	for (int i = 1; i < 100; i++)
	{
		//Sequential reading of images
		std::stringstream s;
		s << "..\\Data\\" << i << ".jpg";
		TFrame frameNext(s.str());
		
		bactList.FrameProcess(frameNext, background, backgroundError, threshold);
		bactList.UpdateList(framePrev, frameNext, minimalBactSize, maximalDistance, 10, 10, 10, 10) ;

		//Color image reading
		cv::Mat mat = cv::imread(s.str(), CV_LOAD_IMAGE_UNCHANGED);
		TViewOpenCV::PaintBactSet(mat, bactList, 3);   
		TViewOpenCV::PaintTracks(mat, bactList, 10, 3);
		
		TViewOpenCV::ShowImage("window1", frameNext.GetImage());
		TViewOpenCV::ShowImage("window2", frameNext.GetProcessedImage());
		TViewOpenCV::ShowImage("window3", mat);
		cv::waitKey();

		framePrev = frameNext ;
	}
}