#include <string>
#include <iostream>
#include <opencv\cv.hpp>
#include <opencv\cxcore.hpp>
#include <opencv\cvaux.hpp>
#include <opencv2\imgproc.hpp>
#include "TFrame.h"


TFrame::TFrame(int width, int height)
{
	image = cv::Mat(width, height, CV_8UC1);
}

TFrame::TFrame(cv::Mat mat)
{
	mat.copyTo(image);
}

TFrame::TFrame(std::string filename)
{
	image = cv::imread(filename, CV_LOAD_IMAGE_GRAYSCALE);
}

TFrame::TFrame(TFrame &frame)
{
	image = frame.GetImage();
	processedImage = frame.GetProcessedImage();
}

TFrame::~TFrame()
{
}

cv::Mat TFrame::GetImage()
{
	if (!image.data)
		return cv::Mat(10, 10, CV_8UC1);
	cv::Mat mat ;
	image.copyTo(mat) ;
	return mat;
}

cv::Mat TFrame::GetProcessedImage()
{
	if (!processedImage.data)
		return cv::Mat(10, 10, CV_8UC1) ;
	cv::Mat mat;
	processedImage.copyTo(mat);
	return mat;
}

void TFrame::DeleteProcessedImage()	//�� ������, ��� ��� �������� ������ ��� ��� � �����
{
	if (!processedImage.data)
		return ;
	processedImage.~Mat() ;
	processedImage.data = nullptr ;
}

cv::Mat TFrame::CalculateBackground(const std::string path, const std::string imgType, 
	const int beginImgNumber, const int endImgNumber)
{
	int imgCount = endImgNumber - beginImgNumber;
	if (imgCount < 1)
		return cv::Mat();
	
	std::stringstream s ;
	s << path << beginImgNumber << imgType ;
	cv::Mat background = cv::imread(s.str(), CV_LOAD_IMAGE_GRAYSCALE) ;

	if (!background.data)
		return cv::Mat();
	background.convertTo(background, CV_32FC1);

	for (int i = beginImgNumber+1 ; i < endImgNumber; i++)
	{
		std::stringstream s;
		s << path << i << imgType;
		cv::Mat mat = cv::imread(s.str(), CV_LOAD_IMAGE_GRAYSCALE);
		mat.convertTo(mat, CV_32FC1);
		background += mat ;
	}

	int size = background.cols*background.rows;
	float* backgroundPtr = (float*)background.ptr();	//Very-very accurate! It is a crutch!
	for (int i = 0; i < size; i++)
		backgroundPtr[i] /= imgCount; 
	background.convertTo(background, CV_8UC1);

	return background;    
}

//http://recog.ru/blog/applied/15.html �������� ��� ��� � �������� ������ ���������� �����������.
//����� ����� ��������� ����������� �����.
void TFrame::Binarization(double maxValue, int adaptiveMethod, int thresholdSize, int blockSize, double C)
{
	if (!image.data)
		return;
	else if (!processedImage.data)
		cv::adaptiveThreshold(image, processedImage, maxValue, adaptiveMethod, thresholdSize, blockSize, C);
	else
		cv::adaptiveThreshold(processedImage, processedImage, maxValue, adaptiveMethod, thresholdSize, blockSize, C);
}

void TFrame::Binarization(int threshold)
{
	if (!image.data)
		return;
	else if (!processedImage.data)
		cv::threshold(image, processedImage, threshold, 255, CV_THRESH_BINARY_INV);
	else
		cv::threshold(processedImage, processedImage, threshold, 255, CV_THRESH_BINARY_INV);
}

void TFrame::Erode(int kernelWidth, int kernelHeight, int kernelShape)
{
	if (!image.data)
		return;
	else if (!processedImage.data)
	{
		cv::Mat kernel = cv::getStructuringElement(kernelShape, cv::Size(kernelWidth, kernelHeight));
		cv::erode(image, processedImage, kernel);
	}
	else
	{	
		cv::Mat kernel = cv::getStructuringElement(kernelShape, cv::Size(kernelWidth, kernelHeight));
		cv::erode(processedImage, processedImage, kernel);
	}

}

void TFrame::Dilate(int kernelWidth, int kernelHeight, int kernelShape)
{
	if (!image.data)
		return;
	else if (!processedImage.data)
	{
		cv::Mat kernel = cv::getStructuringElement(kernelShape, cv::Size(kernelWidth, kernelHeight));
		cv::dilate(image, processedImage, kernel);
	}
	else
	{
		cv::Mat kernel = cv::getStructuringElement(kernelShape, cv::Size(kernelWidth, kernelHeight));
		cv::dilate(processedImage, processedImage, kernel);
	}
}

void TFrame::Blur(int kernelWidth, int kernelHeight)
{
	if (!image.data)
		return;
	else if (!processedImage.data)
		cv::blur(image, processedImage, cv::Size(kernelWidth, kernelHeight));
	else
		cv::blur(processedImage, processedImage, cv::Size(kernelWidth, kernelHeight));
}

void TFrame::MedianBlur(int kernelSize)		//������ ��������� ������ �� ����� ����. ����� ����� ����������, ���� �����������
{
	if (!image.data)
		return;
	else if (!processedImage.data)
		cv::medianBlur(image, processedImage, kernelSize);
	else
		cv::medianBlur(processedImage, processedImage, kernelSize);
}

void TFrame::GaussianBlur(int kernelWidth, int kernelHeight, double sigmaX, double sigmaY)
{
	if (!image.data)
		return;
	else if (!processedImage.data)
		cv::GaussianBlur(image, processedImage, cv::Size(kernelWidth, kernelHeight), 10);
	else
		cv::GaussianBlur(processedImage, processedImage, cv::Size(kernelWidth, kernelHeight), 10);
}

void TFrame::BackgroundCorrection(cv::Mat background, int backgroundError)
{
	if (!image.data)
		return;
	else if (!processedImage.data)
	{
		int size = image.cols*image.rows;
		auto imgPtr = image.ptr();
		auto backPtr = background.ptr();
		for (int i = 0; i < size; i++)
		{
			int value = abs(imgPtr[i] - backPtr[i]);
			if (value < backgroundError)
				value = 0;
			imgPtr[i] = value;
		}
	}
	else
	{
		int size = processedImage.cols*processedImage.rows;
		auto imgPtr = processedImage.ptr();
		auto backPtr = background.ptr();
		for (int i = 0; i < size; i++)
		{
			int value = abs(imgPtr[i] - backPtr[i]);
			if (value < backgroundError)
				value = 0;
			imgPtr[i] = value;
		}
	}
}

cv::Mat TFrame::GetImagePtr()
{
	if (!image.data)
		return cv::Mat(10, 10, CV_8UC1);
	return image;
}

cv::Mat TFrame::GetProcessedImagePtr()
{
	if (!processedImage.data)
		return cv::Mat(10, 10, CV_8UC1);
	return processedImage;
}