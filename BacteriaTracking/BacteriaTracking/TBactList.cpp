#include "TBactList.h"
#include "TFrame.h"

TBactList::TBactList()
{
}

TBactList::TBactList(std::vector<TBact> vec)
{
	data = vec ;
}

TBactList::~TBactList()
{
}

std::vector<TBact> TBactList::GetData()
{
	return data;
}

//We could make this method in TFrame class, but that descision was made
//under influence of passionate wish of creation cool architecture with
//TFrame iharitans. Will see. It can be made in other way
void TBactList::FrameProcess(TFrame & frame, cv::Mat background, int backgroundError, int threshold)
{
	frame.BackgroundCorrection(background, backgroundError) ;
	frame.Binarization(threshold) ;
	frame.GaussianBlur();
	frame.Binarization(65);	//Magic number
	frame.Erode(3, 3);		//TODO need to decrease count of erosion and dilatation
	frame.Dilate(5, 5);
	frame.Dilate(5, 5);
	frame.Dilate(5, 5);
	frame.MedianBlur(21);
}

void TBactList::FindBacteria(TFrame &frame, int minimalBactSize)
{
	if (!data.empty())
		data.clear() ;
	cv::Mat mat = frame.GetProcessedImage() ;	//Copy of processed image from frame
	auto ptr = mat.ptr();
	for (int y = 0; y < mat.rows; y++)
	{
		for (int x = 0; x < mat.cols; x++)
		{
			if (*ptr == 255)					//TODO �������� ��� ������� �������� � ���������� �� ����, ��� ����
			{
				int r = rand() % 256;
				int g = rand() % 256;
				int b = rand() % 256;
				TBact bact = FillBact(mat, cv::Point(x, y), "Bact", cv::Vec3b(r, g, b));
				if (bact.GetSize() >= minimalBactSize)
				{
					data.push_back(bact) ;
				}
			}
			ptr++;
		}
	}
}

//���� �������, ����� ������ maximalDistance �� �� ������������ ����������
//�������� ������� �������� � ����������� �� �� � ����������� �� ����������
void TBactList::UpdateList(TFrame &framePrev, TFrame &frameNext, int minimalBactSize, int maximalDistance,
	int rectWidth, int rectHeight, int transValueX, int transValueY)	//Correlation window parameters
{
	auto lambdaDistance = [](cv::Point p1, cv::Point p2)
	{
		double distance = (p1.x - p2.x)*(p1.x - p2.x) + (p1.y - p2.y)*(p1.y - p2.y);
		return sqrt(distance);
	};
	//Here we think, that frame_prev and frame_next are processed in accordance
	//with FrameProcess with the same settings
	cv::Mat imgPrev = framePrev.GetImage();		//Copies
	cv::Mat imgNext = frameNext.GetImage();
	TBactList nextList ;
	nextList.FindBacteria(frameNext, minimalBactSize) ;

	auto dataNext = nextList.GetData() ;
	for (auto itPrev = data.begin(); itPrev != data.end(); ++itPrev)
	{
		itPrev->SetAliveStatus(false) ;
		double maxCoef = -1 ;
		cv::Point center(-1, -1) ;
		decltype(itPrev) itDel;	//iter for deleting bacteria from next list
		for (auto itNext = dataNext.begin(); itNext != dataNext.end(); ++itNext)
		{
			cv::Point prevCenter = itPrev->GetLastPosition();
			cv::Point nextCenter = itNext->GetLastPosition();
			
			if (nextCenter.x - rectWidth / 2 >= 0 && nextCenter.y - rectHeight / 2 >= 0 &&
				prevCenter.x - rectWidth / 2 >= 0 && prevCenter.y - rectHeight / 2 >= 0 &&
				nextCenter.x + rectWidth / 2 < imgNext.cols && nextCenter.y + rectHeight / 2 < imgNext.rows &&
				prevCenter.x + rectWidth / 2 < imgNext.cols && prevCenter.y + rectHeight / 2 < imgNext.rows)
			{
				cv::Mat prevRect = imgPrev(cv::Rect(prevCenter.x - rectWidth / 2, prevCenter.y - rectHeight / 2, rectWidth, rectHeight));
				cv::Mat nextRect = imgNext(cv::Rect(nextCenter.x - rectWidth / 2, nextCenter.y - rectHeight / 2, rectWidth, rectHeight));				
				double coef = GetCorrelationCoefficient(prevRect, nextRect);

				if (coef > maxCoef && lambdaDistance(prevCenter, nextCenter) < maximalDistance)
				{
					maxCoef = coef;
					center = nextCenter;
					itDel = itNext;
				}
			}
		}
		if (center.x != -1)	//New center was founnd
		{
			itPrev->AddTrackPoint(center) ;
			dataNext.erase(itDel) ;
			itPrev->SetAliveStatus(true)  ;
		}
	}
	for (auto itNext = dataNext.begin(); itNext != dataNext.end(); ++itNext)
		data.push_back(*itNext) ;
}

double TBactList::GetAverageTrackDistance()
{
	return 0.0;
}

double TBactList::GetAverageTrackDistance(double pixelSize)
{
	return 0.0;
}

int TBactList::GetAverageBacteriaSize()
{
	return 0;
}

double TBactList::GetAverageBacteriaSize(double pixelSize)
{
	return 0.0;
}

double TBactList::GetAverageBacteriaSpeed()
{
	return 0.0;
}

double TBactList::GetAverageBacteriaSpeed(double pixelSize, double frameRate)
{
	return 0.0;
}

double TBactList::GetCorrelationCoefficient(cv::Mat & mat1, cv::Mat & mat2)
{
	double m1 = 0;
	double m2 = 0;
	double q1 = 0;	//����� ��������� ��������������� ��������
	double q2 = 0;
	double pr = 0;
	for (int y = 0; y < mat1.rows; y++)
	{
		auto u = mat1.ptr(y);
		auto v = mat2.ptr(y);
		for (int x = 0; x < mat1.cols; x++)
		{
			m1 += u[x];
			m2 += v[x];
			q1 += u[x] * u[x];
			q2 += v[x] * v[x];
			pr += u[x] * v[x];
		}
	}
	int n = mat1.rows*mat1.cols;
	return (n*pr - m1*m2) / (sqrt((n*q1 - m1*m1)*(n*q2 - m2*m2)));
}

//Need other flood fill for acceleration (floodfill with center coordinates calculation)
TBact TBactList::FillBact(cv::Mat & mat, cv::Point point, std::string name, cv::Vec3b color)
{
	if (!mat.data)
		return TBact();
 	int bactSize = 0;
	cv::Point bactCenter(0,0);

	cv::floodFill(mat, point, 127);
	auto ptr = mat.ptr();
	for (int y = 0; y < mat.rows; y++)
	{
		for (int x = 0; x < mat.cols; x++)
		{
			if (*ptr == 127)
			{
				bactCenter.x += x ;
				bactCenter.y += y ;
				bactSize++ ;
			}
			ptr++;
		}
	}
	bactCenter.x /= bactSize ;
	bactCenter.y /= bactSize ;

	cv::floodFill(mat, point, 0);		

	return TBact(bactCenter, bactSize, name, color) ;
}
