#include <opencv\cv.hpp>
#include <opencv\highgui.h>
#include <vector>
#include "TViewOpenCV.h"

TViewOpenCV::TViewOpenCV()
{
}

TViewOpenCV::~TViewOpenCV()
{
}

void TViewOpenCV::PaintTrack(cv::Mat &mat, TBact &bact, const int pointsCount, const int thickness)
{
	if (!mat.data)
		return ;
	if (!bact.GetAliveStatus())
		return; 
	std::vector<cv::Point> track = bact.GetTrack(pointsCount);
	cv::Vec3b color = bact.GetColor();
	for (auto iter = track.begin() + 1; iter != track.end(); ++iter)
		cv::line(mat, *iter, *(iter - 1), color, thickness);
}

void TViewOpenCV::PaintTracks(cv::Mat & mat, TBactList & bactList, 
	const int pointsCount, const int thickness)
{
	std::vector<TBact> data = bactList.GetData();
	for (auto iter = data.begin(); iter != data.end(); ++iter)
		PaintTrack(mat, *iter, pointsCount, thickness);
}

void TViewOpenCV::PaintBact(cv::Mat & mat, TBact & bact, const int thickness, const BactDesignation des)
{
	if (!mat.data)
		return;
	if (!bact.GetAliveStatus())
		return ;
	cv::Point center = bact.GetLastPosition();
	int cx = center.x;
	int cy = center.y;
	cv::Vec3b color = bact.GetColor() ;
	switch (des)
	{
	case BactDesignation::Crosses:
		cv::line(mat, cv::Point(cx - 20, cy - 20), cv::Point(cx + 20, cy + 20), color, thickness);
		cv::line(mat, cv::Point(cx + 20, cy - 20), cv::Point(cx - 20, cy + 20), color, thickness);
		break;
	case BactDesignation::Points:
		//TODO
		break;
	case BactDesignation::Circles:
		//TODO
		break;
	case BactDesignation::Edges:
		//TODO
		break;
	}
}

void TViewOpenCV::PaintBactSet(cv::Mat & mat, TBactList & bactList, const int thickness, const BactDesignation des)
{
	std::vector<TBact> data = bactList.GetData() ;
	for (auto iter = data.begin(); iter != data.end(); ++iter)
		PaintBact(mat, *iter, thickness, des) ;
}

void TViewOpenCV::ShowImage(const std::string windowName, const cv::Mat & mat)
{
	if (!mat.data)
		return;
	cv::imshow(windowName, mat) ;
}

void TViewOpenCV::SaveImage(const std::string fileName, const cv::Mat & mat)
{
	if (!mat.data)
		return;
	cv::imwrite(fileName, mat);
}
