#include <cmath>
#include "TBact.h"

TBact::TBact(cv::Point center, int size, std::string tp, cv::Vec3b c)
	: areaSize(size), type(tp), color(c)
{
	track.push_back(center);
	SetAliveStatus(true) ;
}

TBact::~TBact()
{
}

double TBact::GetTrackDistance()
{
	if (track.size() == 0)
		return 0 ;
}

cv::Vec3b TBact::GetColor()
{
	return color;
}

int TBact::GetSize()
{
	return areaSize;
}

void TBact::SetSize(int size)
{
	areaSize = size ;
}

std::string TBact::GetType()
{
	return type ;	//It is copy of string type
}

bool TBact::GetAliveStatus()
{
	return aliveStatus;
}

void TBact::SetColor(cv::Vec3b c)
{
	color = c ;
}

void TBact::SetColor(uchar r, uchar g, uchar b)
{
	color = cv::Vec3b(r, g, b) ;
}

void TBact::SetType(std::string tp)
{
	type = tp ;
}

void TBact::SetAliveStatus(bool status)
{
	aliveStatus = status ;
}

void TBact::AddTrackPoint(cv::Point center)
{
	track.push_back(center) ;
}

std::vector<cv::Point> TBact::GetTrack(int pointsCount)
{
	if (pointsCount < 1)					//Warning here. TODO remake in unsigned int (size_t)
		return	std::vector<cv::Point>();
	if (track.size() < pointsCount)			//Warning here. TODO remake in unsigned int (size_t)
		pointsCount = track.size() ;
	std::vector<cv::Point> result ;
	auto iter = track.end() - 1 ;
	for (int i = 0; i < pointsCount; i++, iter--)
		result.push_back(*iter) ;
	return result;
}

cv::Point TBact::GetLastPosition()
{
	return *(track.end()-1);
}

double TBact::GetDistance(TBact bact)
{
	cv::Point p1 = GetLastPosition() ;
	cv::Point p2 = bact.GetLastPosition() ;
	double distance = (p1.x - p2.x)*(p1.x - p2.x) + (p1.y - p2.y)*(p1.y - p2.y) ;
	return sqrt(distance) ;
}
